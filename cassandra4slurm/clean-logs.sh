#!/bin/bash
rm -f hostlist-*.txt
rm -f log-cass-*.out
rm -f log-cass-*.err
rm -f snap-status-*.txt
rm -f cassandra-num-nodes.txt cassandra-recover-file.txt cassandra-snapshot-file.txt
rm -f slurm-*.out
rm -f slurm-*.err
